package ru.edu.model;

import org.junit.Test;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;

public class StudentTest {

    Date date = new Date();
    Date date2 = new Date(1980,10,10);
    Student student = new Student("Boris","Jonston",date,true);
    Student student2 = new Student("Boris2","Jonston",date,true);


    @Test
    public void getId() {
        student.setId(UUID.randomUUID());
        assertNotNull(student.getId());
    }

    @Test
    public void getFirstName() {
        assertEquals("Boris", student.getFirstName());
    }

    @Test
    public void getLastName() {
        assertEquals("Jonston", student.getLastName());
    }

    @Test
    public void getBirthDate() {
        assertEquals(date,student.getBirthDate());
    }

    @Test
    public void isGraduated() {
        assertEquals(true,student.isGraduated());
    }

    @Test
    public void setId() {
        student2.setId(UUID.randomUUID());
        assertNotNull(student2.getId());
    }

    @Test
    public void setFirstName() {
        student2.setFirstName("Volodya");
        assertEquals("Volodya", student2.getFirstName());
    }

    @Test
    public void setLastName() {
        student2.setLastName("Pupkin");
        assertEquals("Pupkin", student2.getLastName());
    }

    @Test
    public void setBirthDate() {
        student2.setBirthDate(date2);
        assertEquals(date2,student2.getBirthDate());
    }

    @Test
    public void setGraduated() {
        student2.setGraduated(false);
        assertEquals(false, student2.isGraduated());
    }

    @Test
    public void toStringTest(){
        assertNotNull(student.toString());

    }
}