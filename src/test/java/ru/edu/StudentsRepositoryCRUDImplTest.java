package ru.edu;

import org.junit.Test;
import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class StudentsRepositoryCRUDImplTest {

    java.util.Date date = new java.util.Date();
    java.util.Date date2 = new java.util.Date(1980, 10, 10);
    Student student = new Student("Boris", "Jonston", date, true);
    Student student2 = new Student("Boris2", "Jonston", date2, true);
    Student student3 = new Student();

    public StudentsRepositoryCRUDImplTest() throws SQLException {
    }

    @Test
    public void create() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());

        Student student = new Student("Boris", "Jonson", new Date(Instant.now().getEpochSecond()), false);
        crud.create(student);

        UUID createdId = crud.create(student);

        Student newStudent = crud.selectById(createdId);

        assertEquals(student.getFirstName(), newStudent.getFirstName());
        assertEquals(createdId, newStudent.getId());
    }

    @Test
    public void selectById() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());


        UUID uuid = crud.create(student);
        Student newStudent = crud.selectById(uuid);

        assertEquals(newStudent.getId(),uuid);

    }

    @Test
    public void selectAll() throws SQLException {

        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        UUID createdId = crud.create(student);
        List<Student> list = crud.selectAll();

        assertNotNull(crud.selectAll());


    }

    @Test
    public void update() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        UUID createdId = crud.create(student);

        student.setFirstName("Aleg");
        student.setLastName("Aleg");
        student.setBirthDate(date2);
        student.setGraduated(false);
        student.setId(createdId);

        assertEquals("Aleg",student.getFirstName());

    }

    @Test
    public void remove() throws SQLException {

        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        List<UUID> listId = new ArrayList<>();

        UUID id = crud.create(student);

        listId.add(id);

        assertEquals(1, crud.remove(listId));


    }

    @Test(expected = IllegalStateException.class)
    public void testRemove() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        List<UUID> list = new ArrayList<>();
        list.add(UUID.randomUUID());
        crud.remove(list);

    }

    @Test(expected = NullPointerException.class)
    public void updateEx() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        crud.update(student3);

    }


    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:./target/localDb/db", "sa", "");
    }
}
