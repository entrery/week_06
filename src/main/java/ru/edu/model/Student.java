package ru.edu.model;

import java.util.Date;
import java.util.UUID;

/**
 * Класс отражающий структуру хранимых в таблице полей
 */
public class Student {

    /**
     * Первичный ключ.
     * <p>
     * Рекомендуется генерировать его только внутри StudentsRepositoryCRUD.create(),
     * иными словами до момента пока объект не будет сохранен в БД, он не должен
     * иметь значение id.
     */
    private UUID id;


    /**
     * firstname.
     */
    private String firstName;

    /**
     * lastname.
     */
    private String lastName;

    /**
     * birthdate.
     */
    private Date birthDate;

    /**
     * graduated.
     */
    private boolean isGraduated;

    /**
     * epmpty construct.
     */
    public Student() {
    }

    /**
     * with param.
     *
     * @param firstName1
     * @param lastName1
     * @param birthDate1
     * @param isGraduated1
     */
    public Student(final String firstName1,
                   final String lastName1,
                   final Date birthDate1,
                   final boolean isGraduated1) {
        this.firstName = firstName1;
        this.lastName = lastName1;
        this.birthDate = birthDate1;
        this.isGraduated = isGraduated1;
    }

    /**
     * getid.
     * @return id
     */
    public UUID getId() {
        return id;
    }

    /**
     * get firstname
     * @return firstname
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * get lastname.
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * get burthdate
     * @return birthdate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * graduated
     * @return isGraduated
     */
    public boolean isGraduated() {
        return isGraduated;
    }

    /**
     * set id.
     * @param id1
     */
    public void setId(final UUID id1) {
        this.id = id1;
    }

    /**
     * set first name.
     * @param firstName1
     */
    public void setFirstName(final String firstName1) {
        this.firstName = firstName1;
    }

    /**
     * set last name.
     * @param lastName1
     */
    public void setLastName(final String lastName1) {
        this.lastName = lastName1;
    }

    /**
     * set birthdate.
     * @param birthDate1
     */
    public void setBirthDate(final Date birthDate1) {
        this.birthDate = birthDate1;
    }

    /**
     * set graduated
     * @param graduated1
     */
    public void setGraduated(final boolean graduated1) {
        isGraduated = graduated1;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", isGraduated=" + isGraduated +
                '}';
    }
}
