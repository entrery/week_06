package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class StudentsRepositoryCRUDImpl implements StudentsRepositoryCRUD {

    /**
     * definition.
     */
    public static final String INSERT_STUDENT = "INSERT INTO STUDENTS (id," +
            " first_name, last_name, birth_date, is_graduated) VALUES(?, ?, ?, ?, ?) ";

    /**
     * definition.
     */
    public static final String SELECT_BY_ID = "SELECT * FROM STUDENTS WHERE id = ?";

    /**
     * definition.
     */
    public static final String REMOVE_BY_ID = "DELETE FROM STUDENTS WHERE id = ?";

    /**
     * definition.
     */
    public static final String SELECT_ALL = "SELECT * FROM STUDENTS";

    /**
     * definition.
     */
    public static final String UPDATE = "UPDATE STUDENTS SET first_name=?," +
            " last_name=?,birth_date=?,is_graduated=? WHERE id=?";


    /**
     * connection.
     */
    private Connection connection;

    /**
     * construct.
     *
     * @param connection1
     */
    public StudentsRepositoryCRUDImpl(final Connection connection1) {
        this.connection = connection1;
    }


    /**
     * execute.
     */
    private int execute(String sql, Object... values) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }

            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {
        UUID id = UUID.randomUUID();
        int result = execute(INSERT_STUDENT,
                id.toString(),
                student.getFirstName(),
                student.getLastName(),
                student.getBirthDate(),
                student.isGraduated());
        if (result == 0) {
            throw new IllegalArgumentException(" Not Inserted");
        }
        return id;
    }

    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return Student
     */
    @Override
    public Student selectById(final UUID id) {
        List<Student> query = query(SELECT_BY_ID, id.toString());
        if (query.isEmpty()) {
            return null;
        }
        return query.get(0);

    }

    private List<Student> query(String sql, Object... values) {

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();

            List<Student> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;

        } catch (SQLException e) {

            throw new IllegalStateException(e);
        }
    }

    private Student map(ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(UUID.fromString(resultSet.getString("id")));
        student.setFirstName(resultSet.getString("first_name"));
        student.setLastName(resultSet.getString("last_name"));
        return student;
    }

    /**
     * Получение всех записей из БД.
     *
     * @return
     */
    @Override
    public List<Student> selectAll() {
        List<Student> list = query(SELECT_ALL);
        if (list.isEmpty()) {
            return null;
        }

        return list;
    }

    /**
     * Обновление записи в БД.
     *
     * @param student
     * @return количество обновленных записей
     */
    @Override
    public int update(Student student) {

        UUID id = student.getId();

        int up = execute(UPDATE, student.getFirstName(), student.getLastName(),
                student.getBirthDate(), student.isGraduated(), id.toString());

        if (up == 0) {
            throw new IllegalArgumentException(" value 0 ");
        }

        return up;
    }

    /**
     * Удаление указанных записей по id
     *
     * @param idList
     * @return количество удаленных записей
     */
    @Override
    public int remove(List<UUID> idList) {

        int deleteCount = 0;
        for (UUID uuid : idList) {
            int delete = execute(REMOVE_BY_ID, uuid.toString());
            deleteCount += delete;
        }

        if(deleteCount == 0 ){
            throw new IllegalStateException(" Not Delete");
        }

        return deleteCount;
    }
}
